<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 28.12.2015 17:46
 * File: config.production.inc.php
 * Encoding: UTF-8
 * Project: WebStatic-Boilerplate
 * */

// Initialize global configuration variables
define( 'WebStatic\TEMPLATE_PATH', DOCUMENT_ROOT . '/template/' );
define( 'WebStatic\CONTENT_PATH', DOCUMENT_ROOT . '/content/' );
define( 'WebStatic\SCRIPT_PATH', DOCUMENT_ROOT . '/view/' );
define( 'WebStatic\LANGUAGE_PATH', DOCUMENT_ROOT . '/language/' );

// Initialize local configuration variables
define( 'WebStatic\DEFAULT_SITE_AUTHOR', 'Teslasoft - Christian Kusmanow' );
define( 'WebStatic\NAVBAR_IDENTIFIER', 'navbar' );
define( 'WebStatic\CONTENT_ELEMENT_TAG', 'div' );
define( 'WebStatic\CONTENT_ELEMENT_IDENTIFIER', 'page-contents' );
define( 'WebStatic\DIALOG_IDENTIFIER', 'modal' );

define( 'SITE_NAME', 'Teslasoft.de' );
define( 'SITE_DOMAIN', 'boilerplate.dev.teslasoft.de' );
define( 'SITE_AUTHOR', WebStatic\DEFAULT_SITE_AUTHOR );

define( 'SITE_COMPANY', 'Teslasoft' );
define( 'SITE_COMPANY_HTML', 'TESL<font class="aa">Å</font>SOFT' );
define( 'SITE_COMPANY_STREET', 'Kaiserstraße 44a' );
define( 'SITE_COMPANY_CITY', 'DE-52146 Würselen' );
define( 'SITE_COMPANY_PHONE', '+49 (0)2405 / 406 59 65' );
define( 'SITE_EMAIL', 'info@teslasoft.de' );
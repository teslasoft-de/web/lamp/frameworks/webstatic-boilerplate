/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : banner.js
 Created    : 2016-08-30 18:29:08 +0200
 Updated    : 2016-08-30 18:15:18 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/*!
 * banner.js - grunt-banner task proxy
 * This plugin updates file headers and preserves the original timestamps.
 * Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>
 *
 */
/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />
//const util = require('util');
var fs = require('fs');
var moment = require('moment');
var grunt = require("grunt");
var GruntTask_1 = require("./GruntTask");
var Grunt;
(function (Grunt) {
    var Banner = (function (_super) {
        __extends(Banner, _super);
        function Banner(config, options) {
            _super.call(this, 'banner', 'Create and execute usebanner tasks.', config, options);
            config._ = ['grunt-banner'];
        }
        Banner.prototype.run = function (task) {
            // get (extended) options
            //var options = <BannerOptions> task.options();
            var target = task.target;
            // Fetch Grunt Config
            var pkg = grunt.config.get('pkg');
            var author = pkg.authors[0]; // TODO: Make authors configurable.
            // File
            var targetKey, name, stats, created, modified;
            this.filter(function (file) {
                targetKey = file;
                var match = file.match(/(.*[/])*([^/]*)\.(\w+)*$/);
                // avoid invalid target identifiers
                if (match)
                    targetKey = ((match[1] ? match[1] : '') + match[2]).replace(/[./_]/g, '-');
                targetKey = target + '-' + targetKey;
                name = file.split('/').pop();
                stats = fs.statSync(file);
                //grunt.log.write(util.inspect(stats));
                //grunt.log.write(util.inspect(stats.mtime));
                created = moment(stats.birthtime).format('YYYY-MM-DD HH:mm:ss ZZ');
                modified = moment(stats.mtime).format('YYYY-MM-DD HH:mm:ss ZZ');
                grunt.config('usebanner.' + targetKey, {
                    options: {
                        position: 'replace',
                        replace: true,
                        banner: '/*\n' +
                            ' This file and its contents are limited to the author only.\n' +
                            ' See the file "LICENSE" for the full license governing this code.\n' +
                            ' Differing and additional copyright notices are defined below.\n' +
                            ' ----------------------------------------------------------------\n' +
                            '    Project    : <%= pkg.name %> - <%= pkg.version %>\n' +
                            '    File       : ' + name + '\n' +
                            '    Created    : ' + created + '\n' +
                            '    Updated    : ' + modified + '\n' +
                            '\n' +
                            '    Author     : ' + author.name + ' <' + author.email + '>\n' +
                            '    Company    : ' + author.company + '\n' +
                            '    Copyright  : © <%= grunt.template.today("yyyy") %> ' + author.company + ', ' + author.name + '\n' +
                            '*/\n',
                        linebreak: true
                    },
                    files: {
                        src: [file]
                    }
                });
                grunt.task.run('usebanner:' + targetKey);
                grunt.registerTask('usebanner-' + targetKey, function () {
                    //stats = fs.statSync(file);
                    //runt.log.write(util.inspect(stats.mtime));
                    // Reset file timestamps
                    fs.utimesSync(file, stats.atime, stats.mtime);
                    //stats = fs.statSync(file);
                    //grunt.log.write(util.inspect(stats.mtime));
                });
                grunt.config('usebanner-' + targetKey + '.' + 'mtime', {});
                grunt.task.run('usebanner-' + targetKey);
            });
        };
        ;
        return Banner;
    }(GruntTask_1.GruntTask));
    Grunt.banner = GruntTask_1.GruntTask.init(Banner);
})(Grunt || (Grunt = {}));
module.exports = Grunt;
//# sourceMappingURL=banner.js.map
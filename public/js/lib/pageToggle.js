/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

// Experimental pager header minimum height.
/*var setPageHeaderMinHeight = function(){
    $('.page-header').css('min-height', $(window).height() - $('slide-nav').height());
};
setPageHeaderMinHeight();
$(document).on('throttledresize', setPageHeaderMinHeight);*/

$(document).ready(function() {
    // TODO: Extend so also child page items will open.
    // Trigger hash id link clicks so corresponding collapsed page headers will open.
    var hashID = smoothScroll.getNavIdFromHash();
    if(hashID.substring(0,1) == '#')
        hashID = hashID.substring(1);

    // Searches for a page toggler link element and clicks the declared click target.
    var findPageToggler = function(hashID){
        var link = $("a[href='#"+hashID+"'].scrollspy-link");
        if(link.length){
            var target = link.find(link.attr('data-target'));
            if($('#'+hashID).length && target.length){
                target.click();
                return true;
            }
        }
        return false;
    };

    // If no page toggler found, toggle the hash id element's parent page hader and scroll to hash id.
    if(!findPageToggler(hashID)){
        target = $("[id='"+hashID+"'].scrollspy-target");
        if(target.length){
            target = target.closest('.page-header');
            var match = target.attr("class").match(/page-header-toggle-([\w-]+)\b/);
            if(match){
                togglePageHeaders(match[1]);
                smoothScroll.scrollToElement('#'+hashID);
                return;
            }
        }
        smoothScroll.loaded();
    }
});

// TODO: Open pages from child anchor calls
/* Collapsing Page Headers */
if(!$(smoothScroll.getNavIdFromHash()).length)
    $("[class|='page-header-collapse'], [class*=' page-header-toggle-']").hide();
updateScrollSpy(false);

function togglePageHeaders( id ){
    $("[class='page-header-toggle-"+id+"'], [class~='page-header-toggle-"+id+"']").each( function(i,o){
        $(o).show();
    });
    $("[class|='page-header-collapse']:not(.page-header-toggle-"+id+"), [class*=' page-header-toggle-']:not(.page-header-toggle-"+id+")").each( function(i,o){
        $(o).hide();
    });
    toggleLastPageHeader();
    setTimeout( function(){
        updateScrollSpy(true);
    },0);
}
function toggleLastPageHeader(){
    $('.page-header-last').removeClass('page-header-last');
    $('.page-header').not(':hidden').last().addClass('page-header-last');
}
toggleLastPageHeader();

$('.img-dropshadow-toggle').on('click',function(){
    $(this).addClass( 'img-dropshadow' );
    $('.img-dropshadow-toggle:not(#'+$(this).attr('id')+')').removeClass('img-dropshadow');
});
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: scrollToTop.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

var animationsSupported = !$('html').hasClass('lt-ie9') && Modernizr.cssanimations;
$(document).ready(function() {
    /* Bottom fixed scroll to top button. */
    // Fade in / Fade out when the jumbotron is hidden / visible.
    var scrollToTop = $('.scroll-to-top');
    var scrollToTopOffset = function(){ return $('.jumbotron').outerHeight(); };
    var scrollToTopFadeDuration = 400;
    var scrollTopHighlight = false;
    var fadeScrollToTopInterval;
    var fadeScrollToTop = function(){
        if (!fadeScrollToTopInterval)
            fadeScrollToTopInterval = setInterval(fadeScrollToTop, 500);

        var opacity = 1;
        if(scrollToTop.is('.hide'))
            opacity = 0;
        else if((scrollTopHighlight && scrollToTop.is('.highlight')) || scrollToTop.parent().find('.scroll-to-top:hover').length > 0 || scrollToTop.parent().find('.scroll-to-top:focus').length > 0){
            scrollTopHighlight = false;
            opacity = 1;
        }
        else if(scrollToTop.is('.visible')) {
            opacity = 0.2;
            clearInterval(fadeScrollToTopInterval);
            fadeScrollToTopInterval = null;
        }
        if(scrollToTop.css('opacity') != opacity)
            scrollToTop.css('opacity',opacity);
    };
    scrollToTop.on('mouseover', function (e) {
        fadeScrollToTop();
    });

    var currentScrollTop = $(window).scrollTop();
    var scrollDirection;
    $(window).on('scroll', function(e) {
        // Don't track scroll changes during slie-nav is active.
        if($('body').hasClass('slide-active'))
            return;

        if (!fadeScrollToTopInterval)
            fadeScrollToTop();

        var scrollTop = $(window).scrollTop();
        scrollDirection = currentScrollTop < scrollTop ?'down':'up';
        currentScrollTop = scrollTop;

        var isHidden = scrollToTop.hasClass('hide');
        if ($(this).scrollTop() > scrollToTopOffset()){
            if(isHidden){
                scrollToTop.removeClass('hide');
                if(!animationsSupported)
                    scrollToTop.stop().fadeTo(scrollToTopFadeDuration,1);
            }

            scrollToTop.addClass('visible');
            switch (scrollDirection){
                case'down':
                    scrollToTop.removeClass('highlight');
                    break;
                case'up':
                    scrollTopHighlight = true;
                    scrollToTop.addClass('highlight');
                    break;
            }
        }
        else if(!isHidden){
            var toggle = function(){
                scrollToTop.removeClass('visible');
                scrollToTop.removeClass('highlight');
                scrollToTop.addClass('hide');
            };
            if(animationsSupported)
                toggle();
            else
                scrollToTop.fadeOut(scrollToTopFadeDuration, toggle);

        }
    });
    if($(window).scrollTop() > scrollToTopOffset()){
        scrollToTop.removeClass('hide');
        scrollToTop.show();
    }

    scrollToTop.click(function(event) {
        event.preventDefault();
        $('.jumbotron').one('smoothScrolled', function(){
            scrollToTop.find('a').blur();
        });
        smoothScroll.scrollToElement('.jumbotron');
        return false;
    });
});
$('.scroll-to-top').addClass('hide');
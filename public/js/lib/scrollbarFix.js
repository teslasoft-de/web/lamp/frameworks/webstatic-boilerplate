/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

// Prevent navbar move while dialog opened.
// Content move has been fixed in Bootstrap 3.2.0.
// -->  Open modal is shifting body content to the left #9855
$(document.body)
    .on('show.bs.modal', function () {
        if (this.clientHeight <= window.innerHeight) {
            return;
        }
        // Get scrollbar width
        var scrollbarWidth = getScrollBarWidth();
        if (scrollbarWidth) {
            $('.navbar-fixed-top').css('padding-right', scrollbarWidth);
        }
    })
    .on('hide.bs.modal', function () {
        $('.navbar-fixed-top').css('padding-right', 0);
    });

function getScrollBarWidth () {
    var fwidth = $(document).width();
    $('html').css('overflow-y', 'hidden');
    var swidth = $(document).width();
    $("html").css("overflow-y", "visible");
    return (swidth - fwidth);
}
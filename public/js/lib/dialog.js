/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/
/**
 * Created by Cosmo on 15.10.2014.
 */
$(document).ready(function() {

    // On open modal dialog wait for menu collapse animation to prevent simultanous triggered animations.
    $('.navbar .container:eq(0) .nav li > a[href^=#]').each( function( i, e ){
        var a = $(e);
        var href = a.attr('href');
        if(!$('div[id='+href+']'))
            return;
        a.attr('href', null);

        // Set dialog label
        var id = href.substring(1) + 'Label';
        a.attr( 'id', id );
        $(href).attr('aria-labelledby', id);

        a.on( 'click', function() {
            setTimeout( function() {
                $(href).modal();
                window.document.location = href;
            }, 300 );
        });
    } );

    // Blur page content while dialog shown.
    $('.modal').on('show.bs.modal', function(e) {
        if(!dialogSwitch)
            blur('#page-contents', true);
    }).on('hide.bs.modal', function(e) {
        if(!dialogSwitch && Response.band(768))
            blur('#page-contents', false);
    });

    // Enable dialog to dialog opening.
    dialogSwitch = false;
    $('.modal-body a[href^=#]').on('click', function(e){
        var a = $(this);
        var dialogID = a.attr('href');
        // Cancel if link target is not element id.
        if(!$(dialogID).hasClass('modal'))
            return;
        dialogSwitch = true;
        a.closest('.modal').one('hidden.bs.modal', function(e) {
            setTimeout( function(){
                $(dialogID).modal();
                window.document.location = dialogID;
                dialogSwitch = false;
            }, 0 );

        }).modal('hide');
    });
});
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

/* Set bot-invisible email link targets. */
$("a[href='mailto:']").one( 'click', function() {
    var that = $(this);
    that.attr(
        'href',
        that.attr('href') +
        that.children('span').attr('prepend') +
        that.text().trim() +
        that.children('span').attr('append'));
} );
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

$(document).ready(function() {
    // Setup sitemap modal links
    $('a[href^=#].sitemap-link').each( function(e) {
        var a = $(this);
        var href = a.attr('href');
        if(!$('div[id='+href+']'))
            return;
        a.attr('href', null);

        a.on( 'click', function() {
            $(href).modal();
            window.document.location = href;
        });
    });
});
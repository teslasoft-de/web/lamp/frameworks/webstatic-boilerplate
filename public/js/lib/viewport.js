/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

//#############################################################################
// Browser detection and viewport adjustments
//#############################################################################

function getAndroidVersion(userAgent) {
    var ua = userAgent || navigator.userAgent;
    var match = ua.match(/Android\s([\d\.]*)/);
    return match ? match[1].split('.') : false;
}
var androidVersion = getAndroidVersion();

// Disable zoom on all mobile devices

// Chrome & Android >= 4
if(!!window.chrome || androidVersion && (androidVersion[0] >= 4))
    $('meta[name=viewport]').attr('content','initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no');
// Android <= 4
else if(androidVersion && (androidVersion[0] < 4))
    $('meta[name=viewport]').attr('content','initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height,target-densitydpi=device-dpi,user-scalable=yes');
// IE 10
else if (/IEMobile\/10\.0/.test( navigator.userAgent )) {
    // Workaround for Internet Explorer 10 doesn't differentiate device width from viewport width.
    var msViewportStyle = document.createElement('style');
    msViewportStyle.appendChild(
        document.createTextNode(
            '@-ms-viewport{width:auto!important}'
        )
    );
    document.querySelector('head').appendChild(msViewportStyle);
}
// iOS/Safari
else if(/(iPad|iPhone|iPod)/g.test( navigator.userAgent )){
    $('meta[name=viewport]').attr('content','initial-scale=1.0,width=device-width,user-scalable=0');
}
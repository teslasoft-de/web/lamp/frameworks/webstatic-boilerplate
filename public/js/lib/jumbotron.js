/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

$(document).ready(function() {
    $.fn.fitText = function( compressor, options ) {

    // Setup options
    var comp = compressor || 1;
    var settings = $.extend({
      'minFontSize' : Number.NEGATIVE_INFINITY,
      'maxFontSize' : Number.POSITIVE_INFINITY
    }, options);

    return this.each(function(){
      // Store the object
      var $this = $(this);
      // Resizer() resizes items based on the object width divided by the compressor * 10
      var resizer = function () {
        $this.css('font-size', Math.max(Math.min($this.width() / (comp*10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
      };
      // Call once to set.
      resizer();
      // Call on resize. Opera debounces their resize by default.
      $(window).on('throttledresize.fittext orientationchange.fittext', resizer);
    });
  };
          
    $('.jumbotron h1').fitText( 1, {maxFontSize: '63'} );
    $('.jumbotron h2').fitText( 1.5, {maxFontSize: '36'} );
    $('.jumbotron h3').fitText( 1.75, {maxFontSize: '24'} );
    $('.jumbotron h4').fitText( 2, {maxFontSize: '14'} );
    // Hide Jumbotron headline messages for slide in effect.
    $('.jumbotron h1, .jumbotron h2, .jumbotron h3, .jumbotron h4').hide();
  
    // Setup Jumbotron parallax effect.
    $('.jumbotron').parallax( {
        valueCallback: function( yPos ){
        if(!Response.band(992))
            return [ "70%", yPos ];
        },
        parallaxCallback: function(target, yBgPosition, height, scrollTop){
            // Shift also absolute positioned energy-glow-container canvas.
            target.find('*').filter(function() {
                return $(this).css("position") === 'absolute';
            }).each(function(i,o){
                var e = $(o);
                // Adjust container top
                e.css({'top': parseInt(e.attr('offset-top')) + target.offset().top + yBgPosition + 'px'});

                // Shrink canvas to view port size.
                var canvas = e.find('canvas');
                var canvasHeight = canvas[0].height - 12;
                if(height - scrollTop < canvasHeight + 30){
                    var jHeight = height / 2 - scrollTop;
                    canvas.css({'max-height': (canvasHeight+jHeight)/2 + canvasHeight/2 + 'px'});
                }
                else
                    canvas.css({'max-height': canvas[0].height + 'px'});
            });
        }
    });
    
    function drawGlowGradient( id, alphaMin, alphaMax ){
        var canvas = $(id);
        if(!canvas.length)
            return;
        canvas = canvas[0];
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.rect(0, 0, canvas.width, canvas.height);
        var grd = context.createLinearGradient(0, 0, canvas.width, 0);
        grd.addColorStop(0, 'rgba(255,255,255,'+alphaMin+')');
        grd.addColorStop(0.4, 'rgba(255,255,255,'+(alphaMax-0.1)+')');
        grd.addColorStop(0.5, 'rgba(255,255,255,'+alphaMax+')');
        grd.addColorStop(0.6, 'rgba(255,255,255,'+(alphaMax-0.1)+')');
        grd.addColorStop(1, 'rgba(255,255,255,'+alphaMin+')');

        context.fillStyle = grd;
        context.fill();
    }
    drawGlowGradient('#energy-glow', 0.0, 0.8);

    if($('html').hasClass('lt-ie9') || !Modernizr.cssanimations){
        var increment = 0.05;
        var alphaTransition = function(){
            var container = document.getElementById('energy-glow-container');
            var alpha = container.style.opacity ? Number(parseFloat(container.style.opacity).toFixed(2)) : 0;
            alpha += increment;
            if(alpha === 0 || alpha === 1)
                increment *= -1;
            container.style.opacity = alpha;
            container.style.filter = "alpha(opacity="+alpha*100+")"; // IE8
        };
        setInterval(alphaTransition, 40);
    }
    
    var toggleEnergyGlow = function(){
        
        var energyGlow = $('#energy-glow-container');
        var glowContainer = energyGlow.parent();
        var slideNav = $('#slide-nav');
        var energyGlowHtml = energyGlow.outerHTML();
        
        // Turn glow off before slide-nav is opening.
        energyGlow.addClass('opacityTransition');
        slideNav.on('toggle', function(e, slideActive){
            if(!slideActive){
                var energyGlow = $('#energy-glow-container');
                energyGlow.removeClass('opacityTransition');
                energyGlowHtml = energyGlow.outerHTML();
                energyGlow.remove();
            }
        });
        // Turn glow on before slide-nav is closed.
        slideNav.on('toggled', function(e, slideActive){
            if(!slideActive){
                glowContainer.prepend(energyGlowHtml);
                var energyGlow = $('#energy-glow-container');
                drawGlowGradient('#energy-glow', 0.0, 0.8);
                energyGlow.addClass('opacityTransition');
            }
        });
    };
    setTimeout(function(){
        $('.jumbotron').addClass('opacity-in');
        setTimeout(function(){
            toggleEnergyGlow();
            // Slide down and unblur Jumbotron headline messages.
            
            var blurIn = function( selector, blurLevel, delay, duration ){
                var target = $(selector);
                if(target.length){
                    setTimeout( function() {
                        blur( target, blurLevel );
                        target.addClass( 'slideDown' );
                        blur( target, 0, function(){
                            var id = setInterval(function(){
                                if(Math.round(target.css('opacity')) == 1){
                                    target.removeClass( 'slideDown' );
                                    clearInterval(id);
                                }
                            },500);
                        }, duration );
                        target.show();
                    }, delay );
                }
            };
            blurIn('.jumbotron h4, .jumbotron h4', 5, 0, 1500 );
            blurIn('.jumbotron h3, .jumbotron h3', 6, 0, 1500 );
            blurIn('.jumbotron h2, .jumbotron h2', 7, 1250, 1500 );
            blurIn('.jumbotron h1, .jumbotron h1', 8, 1250, 1500 );
        },500);
    });
});
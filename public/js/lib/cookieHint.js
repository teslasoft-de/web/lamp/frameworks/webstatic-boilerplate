/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/
/**
 * Created by Cosmo on 15.10.2014.
 */
// Check if the cookie hint has been closed.
if( $.cookie('cookie-hint') === 'closed' )
    $('#cookie-hint').hide();

// Set cookie and remember for 7 days that the cookie hint has been closed.
$('#cookie-hint button').click(function( e ){
    e.preventDefault();
    $.cookie('cookie-hint', 'closed', { path: '/', expires: 7 });
});
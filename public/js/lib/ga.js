/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/
/**
 * Created by Cosmo on 15.10.2014.
 */
$(document).ready(function() {
    if (!window.ga)
        return;
// Toggle google analytics
    toggleGAOpt = function () {
        $('#ga-disable').css('visibility', !window.gaEnabled() ? 'hidden' : 'visible');
        $('#ga-disable').css('display', !window.gaEnabled() ? 'none' : 'inline-block');
        $('#ga-enable').css('visibility', window.gaEnabled() ? 'hidden' : 'visible');
        $('#ga-enable').css('display', window.gaEnabled() ? 'none' : 'inline-block');
    };
    $('#ga-disable').on('click', function () {
        gaOptOut();
        toggleGAOpt();
        return false;
    });
    $('#ga-enable').on('click', function () {
        gaOptIn();
        toggleGAOpt();
        return false;
    });
    toggleGAOpt();
});
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

//#############################################################################
// Navbar
//#############################################################################

// Add slidedown animation to site menu dropdowns
$(document).ready(function() {
    var $dropDown = $('.navbar .dropdown'),
        $toggle = $('.navbar .dropdown-toggle');

    // Initialize dropdown plugin
    $toggle.dropdown();

    // Handle menu dropdown click events.
    $dropDown.on('show.bs.dropdown', function(e) {
        var $dropdown = $(this);
        $dropdown.find('.dropdown-menu').first()
            .stop(true, true).slideToggle(400, function () {
                $toggle.each(function (i, e) {
                    var $e = $(e);
                    var $parent = $e.closest('.dropdown');
                    if (!$parent.is($dropdown) && $parent.is('.open'))
                        $e.trigger('click');
                });
            });
    });

    // Remove .open class after animation complete to prevent showing desktop
    // styles in responsive view.
    $dropDown.on('hide.bs.dropdown', function () {
        var $dropDown = $(this),
            $menu = $dropDown.find('.dropdown-menu').first();
        $menu.stop(true, true).slideUp(300, function () {
            var $parent = $dropDown.closest('.open');
            $parent.removeClass('open');
            $menu.css('display', 'none');
        });

        // Prevent bootstrap from removing .open class immediately after click.
        return false;
    });

    // Close all menus on content click.
    $('#page-contents').on('mousedown', function(e) {
        $toggle.each( function( i, e ){
            var $e = $(e);
            var $parent = $e.closest('.dropdown');
            if($parent.is( '.open' ))
                $e.dropdown('toggle');
        });
    });
});

/**
 * Disable user selection and display menu glyphicons in responsive view.
 */
function changeMenuResponsiveStyle()
{
    if ( Response.band(768) )
        $('#slide-nav.navbar').enableSelection();
    else
        $('#slide-nav.navbar').disableSelection();
}
changeMenuResponsiveStyle();

$(window).on('throttledresize', function() {
    if ( Response.band(1200) )
    {
        // 1200+
    }
    else if ( Response.band(992) )
    {
        // 992+
    }
    else if ( Response.band(768) )
    {
        // 768+
    }
    else
    {

    }
    changeMenuResponsiveStyle();
});
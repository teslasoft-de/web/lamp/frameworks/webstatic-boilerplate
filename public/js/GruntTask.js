/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />
"use strict";
//const util = require('util');
var grunt = require("grunt");
var Grunt;
(function (Grunt) {
    var GruntTask = (function () {
        function GruntTask(_name, _description, _config, options) {
            this._name = _name;
            this._description = _description;
            this._config = _config;
            // set default options
            this.options = options;
        }
        Object.defineProperty(GruntTask.prototype, "name", {
            get: function () { return this._name; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntTask.prototype, "description", {
            get: function () { return this._description; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntTask.prototype, "task", {
            get: function () { return this._task; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntTask.prototype, "config", {
            get: function () { return this._config; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntTask.prototype, "options", {
            /**
             * Retuns (extended) task options
             * @returns {TOptions}
             */
            get: function () { return this._task.options(); },
            /**
             * Sets the options object of this task within the grunt config.
             * @param value
             */
            set: function (value) { this._config[this._name]['options'] = value; },
            enumerable: true,
            configurable: true
        });
        GruntTask.init = function (task) {
            return function (config, options) {
                var _task = new task(config, options);
                // register task
                grunt.registerMultiTask(_task._name, _task._description, function () {
                    (function (task) {
                        _task._init(task);
                    })(this);
                });
            };
        };
        GruntTask.prototype._init = function (task) {
            this._task = task;
            this.run(task);
        };
        GruntTask.prototype.filter = function (callback) {
            // iterate through target files (tasks)
            return this.task.files.forEach(function (file) {
                (file.src.length > 0 ? file.src : file.orig.src).filter(function (filepath) {
                    //grunt.log.write(util.inspect(filepath));
                    if (!grunt.file.exists(filepath)) {
                        grunt.log.warn('Source file "' + filepath + '" not found.');
                        return false;
                    }
                    return true;
                }).forEach(callback);
            });
        };
        return GruntTask;
    }());
    Grunt.GruntTask = GruntTask;
})(Grunt || (Grunt = {}));
module.exports = Grunt;
//# sourceMappingURL=GruntTask.js.map
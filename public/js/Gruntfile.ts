/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

module Grunt
{
    export class Teslasoft
    {
        constructor(grunt: grunt)
        {
            const config = new (require('./GruntConfig').GruntConfig)(
                grunt, 'lib/', 'dist/');

            config._banner(config._sourceDir('*.js'));
            config._jshint();
            config._jscs();
            config._concat(config._sourceDir([
                'viewport.js',
                'scrollspy.js',
                'navbar.js',
                'login.js',
                'cookieHint.js',
                'pageToggle.js',
                'dialog.js',
                'scrollbarFix.js',
                'scrollToTop.js',
                'ga.js',
                'jumbotron.js',
                'bot-invisible.js',
                'sitemap.js']));

            config._minify({'<%= outDir %><%= outFile %>.min.js': ['<%= concat.dist.dest %>,<%= concat.dist.src %>']});

            config._watch();
            config._concurrent();

            config.init();

            grunt.registerTask('filebanner', ['banner']);
            grunt.registerTask('build', ['jshint', 'concat', 'minify', 'watch']);
            //grunt.registerTask('default', ['concurrent:check', 'concurrent:build']);
        }
    }
}
export = Grunt.Teslasoft;
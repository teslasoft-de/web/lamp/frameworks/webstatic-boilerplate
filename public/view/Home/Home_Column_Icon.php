<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 07.06.2014
 * File: Home_Column_Description.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

use AppStatic\Core\String;
use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

// Find menu item by column title and get the icon.

foreach ($this->Parent as $key => $value)
    if (String::EndsWidth( $key, '- Title' ))
        break;

/* @var $menuItem WebStatic\MenuItem */
$menuItem = $this->Page->getMenu()->getSite()->getMenu()->FindItem( $value->getValue() );

if (!$menuItem || !$menuItem->getIcon())
    return;

XmlUtility::SetClassAttribute( $this->DOMNode, 'glyphicon*', false );
XmlUtility::SetClassAttribute( $this->DOMNode, '(^|\s+)((fa)|(fa-*))', false );
XmlUtility::SetClassAttribute( $this->DOMNode, $menuItem->getIcon(), $menuItem->getIcon() );

$this->ScriptHandled = true;
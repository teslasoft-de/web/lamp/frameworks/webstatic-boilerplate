<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 07.06.2014
 * File: Home_Column_Details.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

use AppStatic\Core\String;
/* @var $this WebStatic\Core\Content */

// Find page by column title and get the link to it.

// Cancel if not a link target
if(!$this->XPath || !preg_match("~(^|\/)a($|\[)~", $this->XPath))
    return;

foreach ($this->Parent as $key => $value)
    if (String::EndsWidth( $key, '- Title' ))
        break;

$menuItem = $this->Page->getMenu()->getSite()->getMenu()->FindItem( $value->getValue() );

if (!$menuItem)
    return;

$a = $this->Page->getTemplate()->getDOMXPath()->query( $this->XPath, $this->DOMNode )->item( 0 );

$href = $a->attributes->getNamedItem( 'href' );
if(!$href)
    $href = $a->appendChild( $a->ownerDocument->createAttribute( 'href' ) );
$href->nodeValue = $menuItem->getQueryPath();

$title = $a->attributes->getNamedItem( 'title' );
if(!$title)
    $title = $a->appendChild( $a->ownerDocument->createAttribute( 'title' ) );

AppStatic\Data\XmlUtility::SetHtmlContent( $title, $this->Value );

$lang = $this->getPage()->getMenu()->getSite()->getUserDefinedLanguage()
            ? $this->getPage()->getMenu()->getSite()->getUserDefinedLanguage()
            : $this->getPage()->getMenu()->getSite()->getUserLanguage();
switch($lang){
    case 'en':
        $a->nodeValue = 'More »';
        break;
    case 'de':
        $a->nodeValue = 'Mehr »';
        break;
}


$this->ScriptHandled = true;
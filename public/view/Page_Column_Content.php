<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 11.07.2014
 * File: Page_Column_Content.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

use WebStatic\Core\Site;
/* @var $this WebStatic\Core\Content */

// Convert text email to link
$this->Value = Site::GenerateEmailBotInvisibleLinks( $this->Value );

if(empty($this->Value))
    $this->NodeHandled = true;
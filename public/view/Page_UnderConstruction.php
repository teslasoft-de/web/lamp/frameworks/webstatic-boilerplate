<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 01.07.2014
 * File: Page_UnderConstruction.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

// Hide under construction notice

if(!$this->getPage()->getUnderConstruction())
    $this->NodeHandled = true;
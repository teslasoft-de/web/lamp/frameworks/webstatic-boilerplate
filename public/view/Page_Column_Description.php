<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 07.06.2014
 * File: Page_Column_Details.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

/* @var $this WebStatic\Core\Content */

// Set page description
AppStatic\Data\XmlUtility::SetHtmlContent( $this->getDOMNode(), $this->getPage()->getDescription() );

$this->ScriptHandled = true;
<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 21.07.2014
 * File: Page_Footer_GooglePlus_Title.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

// Set Google+ title

$a = $this->Page->getTemplate()->getDOMXPath()->query( $this->XPath, $this->DOMNode )->item( 0 );

$href = $a->attributes->getNamedItem( 'title' );
if(!$href)
    $href = $a->appendChild( $a->ownerDocument->createAttribute( 'title' ) );
$href->nodeValue = $this->Value;

$this->ScriptHandled = true;
<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 02.06.2014
 * File: Page_Footer_Copyright.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

/* @var $this WebStatic\Core\Content */

// Format the copyright string

// Get page created year and append modified year of youngest content element.
$modified = \WebStatic\getLastModified( $this->getPage()->getMenu()->getSite() );

$createdYear = date_parse( $this->getPage()->getCreated() )[ 'year' ];
$modifiedYear = date_parse( $modified )[ 'year' ];
if ($modifiedYear > $createdYear)
    $createdYear = "$createdYear-$modifiedYear";
$this->Value = <<<EOT
<i class="glyphicon glyphicon-copyright-mark"></i> $createdYear $this->Value
EOT
        ;
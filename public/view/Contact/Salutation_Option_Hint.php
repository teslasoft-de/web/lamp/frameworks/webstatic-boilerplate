<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 03.07.2014
 * File: Salutation_Option_Hint.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

if($_SESSION[ 'inputSalutation' ] == 0)
    XmlUtility::SetAttribute( $this->getDOMNode(), 'selected', 'selected' );
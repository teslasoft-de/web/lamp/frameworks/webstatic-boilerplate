<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.07.2014
 * File: Contact_GoogleMap.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

if($this->getPage()->getLanguage() == 'de')
    return;
$domNode = $this->getDOMNode();
XmlUtility::SetAttribute( $domNode, 'data-zoom', 5);
XmlUtility::SetAttribute( $domNode, 'data-latitude', $domNode->attributes->getNamedItem('data-marker-latitude')->nodeValue -0.5);
XmlUtility::SetAttribute( $domNode, 'data-longitude', $domNode->attributes->getNamedItem('data-marker-longitude')->nodeValue);
<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 04.07.2014
 * File: Human_Label.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

XmlUtility::SetHtmlContent( $this->getDOMNode(),
    preg_replace('~\$~', "<span class=\"bot-invisible\" prepend=\"{$_SESSION[ 'inputHumanX' ]}\" append=\"{$_SESSION[ 'inputHumanY' ]}\">+</span>", $this->Value ) );

$this->ScriptHandled = true;
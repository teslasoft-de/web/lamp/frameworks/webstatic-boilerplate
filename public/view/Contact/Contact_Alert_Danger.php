<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.07.2014
 * File: Contact_Alert_Danger.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

if (!$_SESSION[ 'contactError' ]) {
    $this->NodeHandled = true;
} else {
    if (isset( $_SESSION[ 'inputEmailError' ] ) && is_string( $_SESSION[ 'inputEmailError' ] )) {
        $doc = $this->getDOMNode()->ownerDocument;
        $p = $doc->importNode( $doc->createElement( 'p' ) );
        XmlUtility::SetHtmlContent( $p, utf8_htmlentities( $_SESSION[ 'inputEmailError' ] ) );
        $this->getDOMNode()->appendChild( $p );
    }
}
<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 18.07.2014
 * File: Page_Footer_Twitter.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

// Set Twitter url

$a = $this->Page->getTemplate()->getDOMXPath()->query( $this->XPath, $this->DOMNode )->item( 0 );

$href = $a->attributes->getNamedItem( 'href' );
if(!$href)
    $href = $a->appendChild( $a->ownerDocument->createAttribute( 'href' ) );
$href->nodeValue = $this->Value;

$this->ScriptHandled = true;
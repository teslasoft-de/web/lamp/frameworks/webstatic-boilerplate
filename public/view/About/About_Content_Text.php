<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 19.07.2014
 * File: About_Content_Text.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

if($this->Value != 'Home')
    return;

$menuItem = $this->getPage()->getMenu()->getSite()->getMenu()->FindItem( $this->Value );

$this->Value = $menuItem->getPage( false )->getDescription();
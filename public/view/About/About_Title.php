<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 11.07.2014
 * File: About_Title.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

$this->Value = "$this->Value " . filter_input( INPUT_SERVER, 'HTTP_HOST' );
<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 06.06.2014
 * File: Page_Column.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

// Remove the column class from template.
AppStatic\Data\XmlUtility::SetClassAttribute( $this->DOMNode, '^col-', false );
<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 29.07.2014
 * File: redirect.inc.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

$permanent = array(
    '' => ''
);
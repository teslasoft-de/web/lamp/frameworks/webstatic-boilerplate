<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 08.04.2017
 * File: menu.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use WebStatic\Common\PropertyDictionary;

$language_menu = new PropertyDictionary(
    [
        'Home'                           => 'Startseite',
        'Consumer Software'              => 'Endanwender-Software',
        'IT Services'                    => 'IT-Dienstleistungen',
        'Performance Marketing'          => 'Performance-Marketing',
        'Data Query Systems'             => 'Datenbank-Abfragesysteme',
        'IT Hardware Support Networking' => 'Demnächst: IT-Hardware-Support-Networking',
        'Contact'                        => 'Kontakt',
        'About'                          => 'Info',
        'Legal Notice'                   => 'Impressum',
        'Privacy Policy'                 => 'Datenschutzerklärung|datenschutzerklaerung',
        'Help'                           => 'Hilfe',
        'Browser Support'                => 'Unterstützte Browser',
        'About...'                       => 'Über...'
    ] );
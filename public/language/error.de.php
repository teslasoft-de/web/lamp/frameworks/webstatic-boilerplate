<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 25.08.2016
 * File: error.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use WebStatic\Common\PropertyDictionary;

$language_error = new PropertyDictionary( [
    400 => ['400 Bad Request', 'The request cannot be fulfilled due to bad syntax.'],
    403 => ['403 Forbidden', 'The server has refused to fulfil your request.'],
    404 => ['404 Nicht gefunden', 'Die angeforderte Seite konnte nicht gefunden werden.'],
    405 => ['405 Method Not Allowed', 'The method specified in the request is not allowed for the specified resource.'],
    408 => ['408 Request Timeout', 'Your browser failed to send a request in the time allowed by the server.'],
    500 => ['500 Interner Server Fehler', 'Die Anfrage konnte aufgrund einer unerwarteten Bedingung nicht abgeschlossen werden.'],
    502 => ['502 Bad Gateway', 'The server received an invalid response while trying to carry out the request.'],
    504 => ['504 Gateway Timeout', 'The upstream server failed to send a request in the time allowed by the server.']
]);
$language_error->Title    = 'Interner Server Fehler.';
$language_error->SubTitle = 'Ihre Anfrage konnte nicht bearbeitet werden.';
$language_error->Alert    = 'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal.';
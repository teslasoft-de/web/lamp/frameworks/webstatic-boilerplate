<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 25.08.2016
 * File: error.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use WebStatic\Common\PropertyDictionary;

$language_error = new PropertyDictionary([
    400 => ['400 Bad Request', 'The request cannot be fulfilled due to bad syntax.'],
    403 => ['403 Forbidden', 'The server has refused to fulfil your request.'],
    404 => ['404 Not Found', 'The page you requested was not found on this server.'],
    405 => ['405 Method Not Allowed', 'The method specified in the request is not allowed for the specified resource.'],
    408 => ['408 Request Timeout', 'Your browser failed to send a request in the time allowed by the server.'],
    500 => ['500 Internal Server Error', 'The request was unsuccessful due to an unexpected condition encountered by the server.'],
    502 => ['502 Bad Gateway', 'The server received an invalid response while trying to carry out the request.'],
    504 => ['504 Gateway Timeout', 'The upstream server failed to send a request in the time allowed by the server.']
]);
$language_error->Title    = 'Internal Server Error.';
$language_error->SubTitle = 'Your request could not been processed.';
$language_error->Alert    = 'An error occured. Please try again later.';
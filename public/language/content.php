<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 08.04.2017
 * File: content.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use WebStatic\Common\PropertyDictionary;

$language_content = new PropertyDictionary(
    [
        // '' => '',
    ] );
include ../../etc/config
include ../../etc/mixins
extends ../../etc/main
block append page-vars
    - showNavbar = false;
block append navbar-vars
    //- var showCookieHint = false
block page-content
    #gmap-canvas(data-title='TESLÅSOFT', data-latitude='50.801', data-longitude='6.1085761', data-marker-latitude='50.820422', data-marker-longitude='6.131251', data-zoom='12', style='background-color: #dfd4dc')
        h4.gmap-info-content
            | TESL
            font.aa Å
            | SOFT
    #under-contruction
        .container
            .alert.alert-warning.alert-dismissable.fade.in
                i.fa.fa-wrench.fa-2x
                button.close(type='button', data-dismiss='alert', aria-hidden='true') ×
                span
                    strong This page is currently under maintenance.
    .container
        .row
            .col-lg-12
                h2
                    span.glyphicon.glyphicon-envelope.fa-fw
                    span Contact
                p(style='font-style: italic;')
                    | Contact TESL
                    font.aa Å
                    | SOFT
            .col-sm-12
                .alert.alert-success
                    strong
                        span.fa.fa-paper-plane-o.fa-2x
                        | Success! Message sent.
                .alert.alert-danger
                    strong
                        span.fa.fa-exclamation-triangle.fa-2x
                        | Error! Please check the inputs.
            .col-sm-6(style='margin-bottom: 20px')
                form(role='form', method='post')
                    .well.well-sm
                        strong#required-hint
                            i.fa.fa-check
                            span Required Field
                    .form-group
                        label(for='salutation') Salutation
                        .input-group.has-error
                            span.input-group-addon
                                i.fa.fa-user
                            select#inputSalutation.selectpicker(name='inputSalutation', data-style='btn-danger', data-width='100%', required='')
                                option Please select...
                                option(value='1', data-icon='fa fa-female') Mrs
                                option(value='2', data-icon='fa fa-male') Mr
                            span.input-group-addon
                                i.fa.fa-check.form-control-feedback
                    .form-group
                        label(for='inputName') Name
                        .input-group.has-error
                            span.input-group-addon
                                i.fa.fa-user
                            input#inputName.form-control(name='inputName', type='text', placeholder='Enter your name', maxlength='70', required='')
                            span.input-group-addon
                                i.fa.fa-check.form-control-feedback
                    .form-group
                        label(for='inputEmail') E-Mail
                        .input-group
                            span.input-group-addon
                                font(style='font-size: 20px; font-weight: 700; position: relative; bottom: 0.1em') @
                            input#inputEmail.form-control(name='inputEmail', type='email', placeholder='Enter a valid e-mail', maxlength='253', required='')
                            span.input-group-addon
                                i.fa.fa-check.form-control-feedback
                    .form-group
                        label(for='inputTel') Telephone
                        .input-group
                            span.input-group-addon
                                i.fa.fa-phone(style='font-size: 1.3em')
                            input#inputTel.form-control(name='inputTel', type='tel', placeholder='Enter your telephone number.', maxlength='20')
                    .form-group
                        label(for='inputCompany') Company
                        .input-group
                            span.input-group-addon
                                i.fa.fa-building-o(style='font-size: 1.3em')
                            input#inputCompany.form-control(name='inputCompany', type='text', placeholder='Enter your company name', maxlength='70')
                    .form-group
                        label(for='inputWebsiteURL') Website URL
                        .input-group
                            span.input-group-addon
                                i.glyphicon.glyphicon-link(style='font-size: 1.2em')
                            input#inputWebsiteURL.form-control(name='inputWebsiteURL', type='url', placeholder='Enter a valid URL', maxlength='2000')
                    .form-group
                        label(for='inputSubject') Subject
                        .input-group
                            input#inputSubject.form-control(name='inputSubject', type='text', placeholder='Enter a subject', maxlength='255', required='')
                            span.input-group-addon
                                i.fa.fa-check.form-control-feedback
                    .form-group
                        label(for='inputMessage') Message
                        .input-group
                            textarea#inputMessage.form-control(name='inputMessage', placeholder='Leave your message', maxlength='4096', rows='5', onfocus='this.rows=10;', required='')
                            span.input-group-addon
                                i.fa.fa-check.form-control-feedback
                    .form-group
                        label(for='inputHuman')
                            | What is
                            span.bot-invisible(prepend='4', append='3') +
                            | ? (Spam Protection)
                        .input-group
                            input#inputHuman.form-control(name='inputHuman', placeholder='Proof your are human', maxlength='35', required='')
                            span.input-group-addon
                                i.fa.fa-check.form-control-feedback
                    input(type='hidden', name='contactSubmit', value='1')
                    input#submit.btn.btn-success.pull-right(type='submit', name='submit', value='Submit')
            hr.divider.hidden-sm.hidden-md.hidden-lg(style='clear: both; margin-top: 65px')
            .col-sm-5.col-sm-push-1.col-md-push-1.col-lg-push-1
                address
                    h3 Teslasoft
                    p.lead
                        | Kaiserstraße 44a
                        br
                        | DE-52146 Würselen
                        br
                        | Phone: +49 (0)2405 / 406 59 65
                address
                    strong Email Us
                    br
                    a.link(href='mailto:')
                        i.fa.fa-envelope-o.notranslate
                        span.bot-invisible(prepend='info', append=siteDomain) @
block script
    script(src='/js/vendor/bootstrap-maxlength.min.js')
    script(src='/js/vendor/bootstrap-select.min.js')
    script.
        $('.selectpicker').selectpicker({iconBase: 'fa'});
        $('input[maxlength]').each(function (i, e) {
            $(e).maxlength({
                threshold: 5,
                limitReachedClass: "label label-danger"
            });
        });
        $('textarea[maxlength]').each(function (i, e) {
            $(e).maxlength({
                alwaysShow: true,
                limitReachedClass: "label label-danger"
            });
        });
    script.
        var gmap = $('#gmap-canvas');
        setTimeout(function () {
            gmap.addClass('opacity-in');
        }, 500);
        /* google maps */
        function loadGoogleMap() {
            var style = [
                {
                    stylers: [
                        {hue: "#540e42"},
                        {saturation: -85}
                    ]
                }, {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [
                        {lightness: 100},
                        {visibility: "simplified"}
                    ]
                }, {
                    featureType: "road",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                }];
            gmap.data('gmap-style', style);
            gmap.gmap();
        }
        $.getScript(('https:' == document.location.protocol ? 'https' : 'http') +
                '://maps.google.com/maps/api/js?sensor=false&callback=window.loadGoogleMap&async=2&language=' + $('html').attr('lang').substring(0, 2));

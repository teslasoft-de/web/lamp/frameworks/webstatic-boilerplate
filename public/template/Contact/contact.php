<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 02.07.2014
 * File: contact.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\Xml\XPath;
use WebStatic\Core\Content;
use WebStatic\Core\Page;
use WebStatic\Core\Template;

$contactTemplate = new Template( 'Contact Form', __DIR__ . '/contact.phtml' );
$contactTemplate->Load();
$contactTemplate->Save();

$contactTemplate->Contact_GoogleMap = '//div[@id="gmap-canvas"]';

$contactTemplate->Page_UnderConstruction = '//div[@id="under-contruction"]';

$contactTemplate->Page_Container = '//div[@id="page-contents"]/div[@class="container"]';

$contactTemplate->Page_Container->Page_Row = 'div[@class="row"]';
$contactTemplate->Page_Container->Page_Row->Page_Title_Column = 'div[@class="col-lg-12"]';
$contactTemplate->Page_Container->Page_Row->Page_Title_Column->Page_Column_Icon = 'h2/span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$contactTemplate->Page_Container->Page_Row->Page_Title_Column->Page_Column_Title = 'h2/span[2]';
$contactTemplate->Page_Container->Page_Row->Page_Title_Column->Page_Column_Description = 'p';

$contactTemplate->Page_Container->Page_Row->Contact_Alert_Column = 'div[@class="col-sm-12"]';
$contactTemplate->Page_Container->Page_Row->Contact_Alert_Column->Contact_Alert_Success = 'div[@class="alert alert-success"]';
$contactTemplate->Page_Container->Page_Row->Contact_Alert_Column->Contact_Alert_Danger = 'div[@class="alert alert-danger"]';

$contactTemplate->Page_Container->Page_Row->Contact_Form_Column = 'div[@class="col-sm-6"]';
$contactTemplate->Page_Container->Page_Row->Contact_Form_Column->Contact_Form = "//form";
$contactForm = $contactTemplate->Page_Container->Page_Row->Contact_Form_Column->Contact_Form;
$contactForm->Contact_Required_Hint = "div[1]";

$contactForm->Contact_Input_Salutation = "div[2]";
$contactForm->Contact_Input_Salutation->Salutation_Label = 'label';
$contactForm->Contact_Input_Salutation->Salutation_InputGroup = 'div';
$contactForm->Contact_Input_Salutation->Salutation_InputGroup->Salutation_Select = 'select';
$contactForm->Contact_Input_Salutation->Salutation_InputGroup->Salutation_Select->Salutation_Option_Hint = 'option[1]';
$contactForm->Contact_Input_Salutation->Salutation_InputGroup->Salutation_Select->Salutation_Option_Mrs = 'option[2]';
$contactForm->Contact_Input_Salutation->Salutation_InputGroup->Salutation_Select->Salutation_Option_Mr = 'option[3]';

$contactForm->Contact_Input_Name = "div[3]";
$contactForm->Contact_Input_Name->Name_Label = 'label';
$contactForm->Contact_Input_Name->Name_InputGroup = 'div';
$contactForm->Contact_Input_Name->Name_InputGroup->Name_Input = 'input';
$contactForm->Contact_Input_Name->Name_InputGroup->Name_Input_Placeholder = 'input';

$contactForm->Contact_Input_Email = "div[4]";
$contactForm->Contact_Input_Email->Email_Label = 'label';
$contactForm->Contact_Input_Email->Email_InputGroup = 'div';
$contactForm->Contact_Input_Email->Email_InputGroup->Email_Input = 'input';
$contactForm->Contact_Input_Email->Email_InputGroup->Email_Input_Placeholder = 'input';

$contactForm->Contact_Input_Tel = "div[5]";
$contactForm->Contact_Input_Tel->Tel_Label = 'label';
$contactForm->Contact_Input_Tel->Tel_InputGroup = 'div';
$contactForm->Contact_Input_Tel->Tel_InputGroup->Tel_Input = 'input';
$contactForm->Contact_Input_Tel->Tel_InputGroup->Tel_Input_Placeholder = 'input';

$contactForm->Contact_Input_Company = "div[6]";
$contactForm->Contact_Input_Company->Company_Label = 'label';
$contactForm->Contact_Input_Company->Company_InputGroup = 'div';
$contactForm->Contact_Input_Company->Company_InputGroup->Company_Input = 'input';
$contactForm->Contact_Input_Company->Company_InputGroup->Company_Input_Placeholder = 'input';

$contactForm->Contact_Input_WebsiteURL = "div[7]";
$contactForm->Contact_Input_WebsiteURL->WebsiteURL_Label = 'label';
$contactForm->Contact_Input_WebsiteURL->WebsiteURL_InputGroup = 'div';
$contactForm->Contact_Input_WebsiteURL->WebsiteURL_InputGroup->WebsiteURL_Input = 'input';
$contactForm->Contact_Input_WebsiteURL->WebsiteURL_InputGroup->WebsiteURL_Input_Placeholder = 'input';

$contactForm->Contact_Input_Subject = "div[8]";
$contactForm->Contact_Input_Subject->Subject_Label = 'label';
$contactForm->Contact_Input_Subject->Subject_InputGroup = 'div';
$contactForm->Contact_Input_Subject->Subject_InputGroup->Subject_Input = 'input';
$contactForm->Contact_Input_Subject->Subject_InputGroup->Subject_Input_Placeholder = 'input';

$contactForm->Contact_Input_Message = "div[9]";
$contactForm->Contact_Input_Message->Message_Label = 'label';
$contactForm->Contact_Input_Message->Message_InputGroup = 'div';
$contactForm->Contact_Input_Message->Message_InputGroup->Message_Textarea = 'textarea';
$contactForm->Contact_Input_Message->Message_InputGroup->Message_Textarea_Placeholder = 'textarea';

$contactForm->Contact_Input_Human = "div[10]";
$contactForm->Contact_Input_Human->Human_Label = 'label';
$contactForm->Contact_Input_Human->Human_InputGroup = 'div';
$contactForm->Contact_Input_Human->Human_InputGroup->Human_Input = 'input';
$contactForm->Contact_Input_Human->Human_InputGroup->Human_Input_Placeholder = 'input';

$contactForm->Contact_Submit_Button = 'input[@type="submit"]';

$contactTemplate->Page_Container->Page_Row->Contact_HR = 'hr';

$contactTemplate->Page_Container->Page_Row->Contact_Address_Column = 'div[last()]';
$contactTemplate->Page_Container->Page_Row->Contact_Address_Column->Contact_Address = 'address[1]';
$contactTemplate->Page_Container->Page_Row->Contact_Address_Column->Contact_Address->Contact_Company_Name = 'h3';
$contactTemplate->Page_Container->Page_Row->Contact_Address_Column->Contact_Address->Contact_Company_Address = 'p';
$contactTemplate->Page_Container->Page_Row->Contact_Address_Column->Contact_Email = 'address[2]';
$contactTemplate->Page_Container->Page_Row->Contact_Address_Column->Contact_Email->Contact_Company_Email_Label = 'strong';
$contactTemplate->Page_Container->Page_Row->Contact_Address_Column->Contact_Email->Contact_Company_Email = 'a/span';

$contactTemplate->Page_Footer = '//div[@id="footer"]';
$contactTemplate->Page_Footer->Page_Footer_Container = '//footer';
$contactTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Copyright = 'span[1]';
$contactTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Twitter_URL = 'span[2]';
$contactTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Twitter_Title = 'span[2]';
$contactTemplate->Page_Footer->Page_Footer_Container->Page_Footer_GooglePlus_URL = 'span[3]';
$contactTemplate->Page_Footer->Page_Footer_Container->Page_Footer_GooglePlus_Title = 'span[3]';

function CreateGoogleMap( Page $page )
{
    $page->SetContent( $page->getTemplate()->Contact_GoogleMap, 'Google Map' );
}

function SetContactPageTitle( Content $row, $class, $name, $title )
{
    $column = $row->SetChild(
        'Page_Title_Column', "$name - Page Column", null, null, $class );
    $column->SetChild(
        'Page_Column_Icon', "$name - Page Icon" );
    $column->SetChild(
        'Page_Column_Title', "$name - Page Title", null, $title );
    $column->SetChild(
        'Page_Column_Description', "$name - Page Description" );
}

function AddContactAlertSuccess( Content $row, $title )
{
    $alerts = $row->SetChild( 'Contact_Alert_Column', 'Contact Form - Alert Column' );
    $alerts->SetChild( 'Contact_Alert_Success', "Contact Form - Alert Success", 'strong/text()[last()]', $title );
}

function AddContactAlertDanger( Content $row, $title )
{
    $alerts = $row->SetChild( 'Contact_Alert_Column', 'Contact Form - Alert Column' );
    $alerts->SetChild( 'Contact_Alert_Danger', "Contact Form - Alert Danger", 'strong/text()[last()]', $title );
}

function GetContactForm( Content $row )
{
    $column = $row->SetChild( 'Contact_Form_Column', 'Contact Form - Form Column' );
    return $column->SetChild( 'Contact_Form', 'Contact Form' );
}

function SetRequiredMassage( Content $form, $message )
{
    $form->SetChild( 'Contact_Required_Hint', 'Contact Form - Required Hint', 'strong/span', $message);
}

function SetContactSalutation( Content $form, $inputType, $name, $label, $hint, $mrs, $mr )
{
    $child = $form->SetChild(
        "Contact_Input_$name", "Contact Form - $name");
    $child->SetChild(
        "{$name}_Label", "Contact Form - $name Label", null, $label );
    $group = $child->SetChild(
        "{$name}_InputGroup", "Contact Form - $name Input Group" );
    $select = $group->SetChild(
        "{$name}_$inputType", "Contact Form - $name $inputType" );
    $select->SetChild( "{$name}_Option_Hint", "Contact Form - $name Option Hint", null, $hint );
    $select->SetChild( "{$name}_Option_Mrs", "Contact Form - $name Option Mrs", null, $mrs );
    $select->SetChild( "{$name}_Option_Mr", "Contact Form - $name Option Mr", null, $mr );
}

function SetContactInput( Content $form, $inputType, $name, $label = null, $placeholder = null )
{
    $child = $form->SetChild(
        "Contact_Input_$name", "Contact Form - $name");
    $child->SetChild(
        "{$name}_Label", "Contact Form - $name Label", null, $label );
    $group = $child->SetChild(
        "{$name}_InputGroup", "Contact Form - $name Input Group" );
    $group->SetChild(
        "{$name}_$inputType", "Contact Form - $name $inputType" );
    $group->SetChild(
        "{$name}_{$inputType}_Placeholder", "Contact Form - $name $inputType Placeholder", null, $placeholder );
}

function SetSubmitButton( Content $form, $value )
{
    $form->SetChild( 'Contact_Submit_Button', 'Contact Form - Submit Button', null, $value );
}

function CreateContactHR( Content $row )
{
    $row->SetChild( 'Contact_HR', 'Contact Form - HR' );
}

function SetContactAddress( Content $row, $companyName, $companyAddress )
{
    $column = $row->SetChild( 'Contact_Address_Column', 'Contact Form - Address Column' );
    $address = $column->SetChild( 'Contact_Address', 'Contact Form - Company Address' );
    $address->SetChild( 'Contact_Company_Name', 'Contact Form - Company Name', null, $companyName );
    $address->SetChild( 'Contact_Company_Address', 'Contact Form - Company Location', null, $companyAddress );
}

function SetContactEmail( Content $row, $label, $email )
{
    $column = $row->SetChild( 'Contact_Address_Column', 'Contact Form - Address Column' );
    $address = $column->SetChild( 'Contact_Email', 'Contact Form - Company Email' );
    $address->SetChild( 'Contact_Company_Email_Label', 'Contact Form - Company Email Label', null, $label );
    $address->SetChild( 'Contact_Company_Email', 'Contact Form - Company Email Address', null, $email );
}
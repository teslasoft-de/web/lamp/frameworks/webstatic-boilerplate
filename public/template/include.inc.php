<?php
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 26.08.2016, 13:21
 * File: include.inc.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

use WebStatic\Core\Content;
use WebStatic\Core\Page;

function SetPageHeader( Page $page, $subtitle, $title )
{
    $jumbotron = $page->SetContent( $page->getTemplate()->Page_Jumbotron, 'Jumbotron' );
    $jumbotron->SetChild( 'Page_Jumbotron_SubTitle', 'Subtitle', null, $subtitle );
    $jumbotron->SetChild( 'Page_Jumbotron_Title', 'Title', null, $title );
}

function CreateUnderConstruction( Page $page, $language )
{
    /*switch($language){
        case 'de':
            $message = <<<EOT
<div class="container">
    <div class="alert alert-warning alert-dismissable fade in">
        <i class="fa fa-wrench fa-2x"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <span>
            <strong>Diese Website befindet sich im Testbetrieb und ist noch im Aufbau. Dadurch kann es zu Darstellungsproblemen kommen.</strong><br/>
            Wir werden Sie per Twitter und Google+ zu informieren wenn der Testbetrieb beendet ist und alle Inhalte verfügbar sind. (Die Links zu den sozialen Netzwerken befinden sich unten in der Fußzeile.)
        </span>
    </div>
</div>
EOT;
            break;
        default:
            $message = <<<EOT
<div class="container">
    <div class="alert alert-warning alert-dismissable fade in">
        <i class="fa fa-wrench fa-2x"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <span>
            <strong>This Website is in test operation and still under construction. Thereby display problems may occur.</strong><br/>
            We will inform you by Twitter and Google+ when the test operation has finished and all contents are available. (See the social network links in the footer below.)
        </span>
    </div>
</div>
EOT;
            break;
    }*/
    switch($language){
        case 'de':
            $message = <<<EOT
<strong>teslasoft.de ist im Testbetrieb und befindet sich noch im Aufbau. Dadurch kann es zu Darstellungsproblemen kommen.</strong><br/>
Wir werden Sie per
<a title="@Teslasoft_de" href="https://twitter.com/Teslasoft_de">
    Twitter
</a>
und
<a title="+TeslasoftDe" rel="publisher" href="https://google.com/+TeslasoftDe">
    Google+
</a>
informieren wenn der Testbetrieb beendet ist und alle Inhalte verfügbar sind.
EOT;
            break;
        default:
            $message = <<<EOT
<strong>teslasoft.de is in test operation and still under construction. Thereby display problems may occur.</strong><br/>
We will inform you by
<a title="@Teslasoft_de" href="https://twitter.com/Teslasoft_de">
    Twitter
</a>
and
<a title="+TeslasoftDe" rel="publisher" href="https://google.com/+TeslasoftDe">
    Google+
</a>
when the test operation has finished and all contents are available.
EOT;
            break;
    }
    $page->SetContent( $page->getTemplate()->Page_UnderConstruction, 'Under Construction', 'div/div/span', $message );
}

function GetContentContainer( Page $page )
{
    return $page->SetContent( $page->getTemplate()->Page_Container, 'Container' );
}

function GetRow( Content $container, $name = null )
{
    $row = 'Content Row';
    if($name)
        $row .= " - $name";
    return $container->SetChild( 'Page_Row', $row );
}

function SetFooter( Page $page, $copyright, $socialTitle, $twitterURL = 'https://twitter.com/Teslasoft_de', $googlePlusURL = 'https://google.com/+TeslasoftDe' )
{
    $footer = $page->SetContent( $page->getTemplate()->Page_Footer, 'Footer' );
    $container = $footer->SetChild( 'Page_Footer_Container', 'Footer Container' );
    $container->SetChild( 'Page_Footer_Copyright', 'Footer - Copyright', null, $copyright );
    $container->SetChild( 'Page_Footer_Twitter_URL', 'Footer - Twitter URL', 'a', $twitterURL );
    $container->SetChild( 'Page_Footer_Twitter_Title', 'Footer - Twitter Title', 'a', preg_replace( '~\$~', "Twitter", $socialTitle ) );
    $container->SetChild( 'Page_Footer_GooglePlus_URL', 'Footer - Google+ URL', 'a', $googlePlusURL );
    $container->SetChild( 'Page_Footer_GooglePlus_Title', 'Footer - Google+ Title', 'a', preg_replace( '~\$~', "Google+", $socialTitle ) );
}
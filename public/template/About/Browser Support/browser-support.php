<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.06.2014
 * File: browser-support.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\Xml\XPath;
use WebStatic\Core\Page;
use WebStatic\Core\Template;

$wbsTemplate = new Template( 'Browser Support - Dialog', __DIR__ . '/browser-support.phtml' );
$wbsTemplate->Load();
$wbsTemplate->Save();

$wbsTemplate->WebbrowserSupport_Header = '//div[@class="modal-header"]';
$wbsTemplate->WebbrowserSupport_Header->WebbrowserSupport_Title = 'h3';

$wbsTemplate->WebbrowserSupport_Body = '//div[@class="modal-body"]';
$wbsTemplate->WebbrowserSupport_Body->WebbrowserSupport_Content = 'div';
$wbsTemplate->WebbrowserSupport_Body->WebbrowserSupport_Table = 'div' . XPath::ContainsAttributeValue( 'class', 'panel' );
$wbsTemplate->WebbrowserSupport_Body->WebbrowserSupport_Footer = 'div[@class="modal-footer"]';

function SetWBSTitle( Page $page, $title )
{
    $page->SetContent( $page->getTemplate()->WebbrowserSupport_Header, 'Header' )
        ->SetChild( 'WebbrowserSupport_Title', 'Title', 'text()', $title );
}

function GetWBSBody( Page $page )
{
    return $page->SetContent( $page->getTemplate()->WebbrowserSupport_Body, 'Body' );
}
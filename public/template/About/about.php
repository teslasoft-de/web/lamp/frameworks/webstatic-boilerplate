<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.06.2014
 * File: about.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use WebStatic\Core\Content;
use WebStatic\Core\Page;
use WebStatic\Core\Template;

$aboutTemplate = new Template( 'About - Dialog', __DIR__ . '/about.phtml' );
$aboutTemplate->Load();
$aboutTemplate->Save();

$aboutTemplate->About_Header = '//div[@class="modal-header"]';
$aboutTemplate->About_Header->About_Title = 'h3';

$aboutTemplate->About_Body = '//div[@class="modal-body"]';
$aboutTemplate->About_Body->About_Content = 'p';
$aboutTemplate->About_Body->About_Content->About_Content_Text = 'span';
$aboutTemplate->About_Body->About_Content->About_Content_Link = 'a';

$aboutTemplate->About_Body->About_Frameworks = './/dl[1]';
$aboutTemplate->About_Body->About_CMS = './/dl[2]';
$aboutTemplate->About_Body->About_Footer = './/div[@class="modal-footer"]';

function SetAboutTitle( Page $page, $title )
{
    $page->SetContent( $page->getTemplate()->About_Header, 'Header' )
        ->SetChild( 'About_Title', 'Title', 'text()', $title );
}

function GetAboutBody( Page $page )
{
    return $page->SetContent( $page->getTemplate()->About_Body, 'Body' );
}

function SetAboutContent( Content $body, $text = null, $linkName = null )
{
    $content = $body->SetChild( 'About_Content', "$linkName - Content" );
    $content->SetChild( 'About_Content_Text', "$linkName - Text", null, $text ? $text : 'Home' );
    if($linkName)
        $content->SetChild( 'About_Content_Link', "$linkName - Link", null, $linkName );
}
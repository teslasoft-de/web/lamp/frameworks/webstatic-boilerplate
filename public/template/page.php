<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 22.05.2014
 * File: page.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

use AppStatic\Data\Xml\XPath;
use AppStatic\Data\XmlUtility;
use WebStatic\Core\Content;
use WebStatic\Core\Page;
use WebStatic\Core\Template;
use WebStatic\Core\TemplateItem;

///////////////////////////////////////////////////////////////////////////////
// Page Template
// 

$pageTemplate = new Template( 'Page - Comming Soon', __DIR__ . '/page.phtml' );
$pageTemplate->Load();
$pageTemplate->Save();

$pageTemplate->Page_Jumbotron = '//div' . XPath::ContainsAttributeValue( 'class', 'jumbotron' );
$pageTemplate->Page_Jumbotron->Page_Jumbotron_Title = 'div[@class="container"]/h1';
$pageTemplate->Page_Jumbotron->Page_Jumbotron_SubTitle = 'div[@class="container"]/h3';

$pageTemplate->Page_UnderConstruction = '//div[@id="under-contruction"]';

$pageTemplate->Page_Container = '//div[@id="container"]';
$pageTemplate->Page_Container->Page_Row = 'div[@class="row"]';
$pageTemplate->Page_Container->Page_Row->Page_Column = 'div';
$pageTemplate->Page_Container->Page_Row->Page_Column->Page_Column_Icon = 'h2/span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$pageTemplate->Page_Container->Page_Row->Page_Column->Page_Column_Title = 'h2/span[2]';
$pageTemplate->Page_Container->Page_Row->Page_Column->Page_Column_Description = 'p[1]';
$pageTemplate->Page_Container->Page_Row->Page_Column->Page_Column_Content = 'p[2]';

$pageTemplate->Page_Content = '//div[@id="contents"]';

$pageTemplate->Page_Footer = '//div[@id="footer"]';
$pageTemplate->Page_Footer->Page_Footer_Container = '//footer';
$pageTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Copyright = 'span[1]';
$pageTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Twitter_URL = 'span[2]';
$pageTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Twitter_Title = 'span[2]';
$pageTemplate->Page_Footer->Page_Footer_Container->Page_Footer_GooglePlus_URL = 'span[3]';
$pageTemplate->Page_Footer->Page_Footer_Container->Page_Footer_GooglePlus_Title = 'span[3]';

function AddPageColumn( Content $row, $class, $name, $title, $content = null )
{
    $column = $row->SetChild(
        'Page_Column', "$name - Column", null, null, $class );
    $column->SetChild(
        'Page_Column_Icon', "$name - Icon" );
    $column->SetChild(
        'Page_Column_Title', "$name - Title", null, $title );
    $column->SetChild(
        'Page_Column_Description', "$name - Description" );
    $column->SetChild(
        'Page_Column_Content', "$name - Content", null, $content );
}

function SetPageColumContent( Content $row, $class, $name, $title, $filePath )
{
    $template = new Template( $name, $filePath );
    $template->Load();
    $item = new TemplateItem( $template, 'content', '//div[@id="page-contents"]' );
    $item->Load( false );    
    AddPageColumn( $row, $class, $name, $title, XmlUtility::GetHtmlContent( $item->getDOMNode() ) );
}

function SetPageContent( Page $page, $name, $filePath )
{
    $template = new Template( $name, $filePath );
    $template->Load();
    $item = new TemplateItem( $template, 'content', '//div[@id="page-contents"]' );
    $item->Load( false );
    $page->SetContent( $page->getTemplate()->Page_Content, 'Content', null, XmlUtility::GetHtmlContent( $item->getDOMNode() ) );
}

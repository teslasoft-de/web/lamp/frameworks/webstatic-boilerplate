<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 01.06.2014
 * File: Home.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( 'Teslasoft entwickelt und vertreibt Anwendungs- und Dienstprogramme im IT-Endanwendersegment und bietet Dienstleistungen rund um die Produktentwicklung, sowie den Aufbau und Betrieb von Projektinfrastrukturen für Unternehmen an.' );
$page->setKeywords( 'Teslasoft, Anwendungs-Software, Dienstprogramme, Utility-Software, IT-Dienstleistungen, Produktentwicklung, Endanwender-Software, Datenbank-Abfragesysteme, IT-Consulting, Performance-Marketing, E-Marketing Kampagnen, Web-Hosting, IT-Hardware-Support-Networking, Microsoft Windows, Windows Desktop, c#, .net Framework, php, MySQL, html5, javascript, AppStatic, WebStatic, Treiber-Studio, HardCare, TeslaMail' );
$page->Save();

SetPageHeader( $page, "We make IT life easier.", SITE_COMPANY_HTML );
CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row1 = GetRow( $container, '1' );

AddHomeColumn( $row1, 'col-md-12', 'Consumer Software' );

AddHomeColumn( $row1, 'col-md-4', 'Performance Marketing' );

AddHomeColumn( $row1, 'col-md-4', 'IT-Consulting' );

AddHomeColumn( $row1, 'col-md-4', 'Web-Hosting' );

$row2 = GetRow( $container, '2' );

AddHomeColumn( $row2, 'col-md-5', 'Data Query Systems' );

AddHomeColumn( $row2, 'col-md-7', 'IT Hardware Support Networking' );

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );
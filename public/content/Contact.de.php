<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 08.06.2014
 * File: Contact.de.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( "Wir freuen uns über Ihre Mitteilung und antworten schnellstmöglich." );
$page->setRobots( 'noarchive' );
$page->setGooglebot( 'noarchive' );
$page->setSlurp( 'noarchive' );
$page->setMSNBot( 'noarchive' );
$page->setTeoma( 'noarchive' );
$page->Save();

CreateGoogleMap( $page );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

SetContactPageTitle( $row, 'col-lg-12', 'Contact Form',
    'Kontaktieren Sie uns' );

AddContactAlertSuccess( $row,
    'Versendet! Vielen dank für Ihre Nachricht. Wir werden Ihre Anfrage in Kürze beantworten.' );
AddContactAlertDanger( $row,
    'Fehler! Bitte überprüfen Sie die Eingabefelder.' );

$contactForm = GetContactForm( $row );
SetRequiredMassage( $contactForm,
    'Pflichtfeld' );

SetContactSalutation( $contactForm, 'Select', 'Salutation',
    'Anrede', 'Ihre Anrede...', 'Frau', 'Herr' );

SetContactInput( $contactForm, 'Input', 'Name', 'Name', 'Ihr vollständiger Name' );
SetContactInput( $contactForm, 'Input', 'Email', 'E-Mail', 'Ihre E-Mail-Adresse' );
SetContactInput( $contactForm, 'Input', 'Tel', 'Telefon', 'Ihre Telefonnummer' );
SetContactInput( $contactForm, 'Input', 'Company', 'Firma', 'Ihr Firmenname' );
SetContactInput( $contactForm, 'Input', 'WebsiteURL', 'Website', 'Ihre Website' );
SetContactInput( $contactForm, 'Input', 'Subject', 'Betreff', 'Bitte geben Sie einen Betreff an.' );
SetContactInput( $contactForm, 'Textarea', 'Message', 'Mitteilung', 'Ihre Mitteilung an uns...' );
SetContactInput( $contactForm, 'Input', 'Human', 'Wie viel ist $? (Spam-Schutz)', 'Bitte lösen Sie die Aufgabe.' );
SetSubmitButton( $contactForm, 'Mitteilung senden' );

SetContactAddress( $row, SITE_COMPANY, SITE_COMPANY_STREET . '<br/>' . SITE_COMPANY_CITY . '<br/>Tel: ' . SITE_COMPANY_PHONE );

SetContactEmail( $row, 'Schreiben Sie uns eine Email', SITE_EMAIL );

CreateContactHR( $row );

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );
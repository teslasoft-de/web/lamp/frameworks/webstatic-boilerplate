<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 08.06.2014
 * File: Contact.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( "We are looking forward to your message and will reply as soon as possible." );
$page->setRobots( 'noarchive' );
$page->setGooglebot( 'noarchive' );
$page->setSlurp( 'noarchive' );
$page->setMSNBot( 'noarchive' );
$page->setTeoma( 'noarchive' );
$page->Save();

CreateGoogleMap( $page );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

SetContactPageTitle( $row, 'col-lg-12', 'Contact Form',
    'Contact Us' );

AddContactAlertSuccess( $row,
    'Success! Your message was sent. We will answer your request as soon a possible.' );
AddContactAlertDanger( $row,
    'Failure! Please check the inputs.' );

$contactForm = GetContactForm( $row );
SetRequiredMassage( $contactForm,
    'Required Field' );

SetContactSalutation( $contactForm, 'Select', 'Salutation',
    'Salutation', 'Your salutation...', 'Mrs', 'Mr' );

SetContactInput( $contactForm, 'Input', 'Name', 'Name', 'Your full name' );
SetContactInput( $contactForm, 'Input', 'Email', 'E-Mail', 'Your e-mail address' );
SetContactInput( $contactForm, 'Input', 'Tel', 'Telephone', 'Your telephone number' );
SetContactInput( $contactForm, 'Input', 'Company', 'Company', 'Your company name' );
SetContactInput( $contactForm, 'Input', 'WebsiteURL', 'Website', 'Your Website-URL' );
SetContactInput( $contactForm, 'Input', 'Subject', 'Subject', 'Please specify a subject.' );
SetContactInput( $contactForm, 'Textarea', 'Message', 'Message', 'Leave your message...' );
SetContactInput( $contactForm, 'Input', 'Human', 'What is $? (Spam Protection)', 'Proof your are human.' );
SetSubmitButton( $contactForm, 'Submit Message' );

SetContactAddress( $row, SITE_COMPANY, SITE_COMPANY_STREET . '<br/>' . SITE_COMPANY_CITY . '<br/>Phone: ' . SITE_COMPANY_PHONE );

SetContactEmail( $row, 'Email Us', SITE_EMAIL );

CreateContactHR( $row );

SetFooter( $page, SITE_COMPANY_HTML, 'Follow us on $' );
<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Data Query Systems.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Are you looking for a way to manage and analyze complex business and project data easily? These tasks can be laborious and time consuming if you use inflexible administration tools which must be updated and extended for every eventuality.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Dynamic Data Management" );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'Data Query Systems', 'Data Query Systems', <<<EOT
Business and project data grow constantly during operation and the structure of this data is subjected to continuous adjustments, such as customizing your offer to your customers through gained experience or required changes to the now very fast paced market adjustments.
<hr/>
In traditional database applications with a static database layer, even the smallest changes of the database structure require a time-expensive test and reintroduction stage. In addition, you can end up with a large amount of bugs, which in turn requires a time-expensive troubleshooting.
<hr/>
We can help you to get out of this mess and offer a way to build enterprise in-house administration software and its business object layer easily extendable. With our innovative O/R mapping technology and visual tools you are able to generate and extend your business object queries on your own by traversing your data structure visually. The data needed for your business model can so fully dynamically handled!
EOT
);

SetFooter( $page, SITE_COMPANY_HTML, 'Follow us on $' );

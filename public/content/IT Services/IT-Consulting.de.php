<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: IT-Consulting.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Wir sind Ihr Partner um Ihre Ideen auf den richtigen Weg zu bringen. Von der Marktanalyse über Software-Entwicklungsanforderungen bis hin zu Vertriebsstrategien. Profitieren Sie von unserer jahrelangen Erfahrung in der Produktentwicklung.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Broad IT Knowledge" );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'IT-Consulting', 'IT-Consulting', <<<EOT
Design, Beratung und Umsetzung von Projekten in den Bereichen Endanwender-Software, IT-Infrastruktur, IT-Sicherheit und betriebsinterner Unternehmenssoftware sind ebenso ein Teil unserer täglichen Aufgaben, wie auch der First-, Second- und der Third-Level-Support in Notfällen.
<hr/>
Unabhängig davon verfolgen wir fortdauernd aktuelle Trends der IT um Ihr Portfolio auch zukünftig kunden- und zukunftsorientiert erweitern zu können.
EOT
);

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );

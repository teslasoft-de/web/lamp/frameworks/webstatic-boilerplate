<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Performance Marketing.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Creating and executing successful e-marketing campaigns, is one of core objectives. We offer an efficient address management, detailed performance analysis and a constantly growing selection of audience group characteristics.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Trusted E-Mails" );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );
$domainName = $page->getMenu()->getSite()->getDomain()->getName();
AddPageColumn( $row, 'col-lg-12', 'Performance Marketing', 'Performance Marketing', <<<EOT
To perform successful e-marketing campaigns, not only optimal technical conditions are required. Beginning with an email server infrastructure, which guarantees in compliance with current technologies, a trust among providers. Over campaign monitoring, bounce processing and provider contact, additionally a permanent Deliverability must be ensured. Rather, the offer must be targeted to audiences and subsequently analyzed to ensure the best possible performance for further campaigns in the future.
<hr/>
Participate in our know-how in the area of e-marketing and our knowledge of the market. Furthermore, we offer a high scaled e-mail server architecture and sophisticated performance metrics analysis tools to ensure highest quality campaigns. We offer essential services for successful direct response marketing with average open rates of up to 40%. Benefit from our computer hardware related address pool with more than 200k addresses and thousands of continuous growing audience group attributes. With our toolset you can create, monitor and analyze your campaign statistics easily.
<hr/>
Questions about pricing and booking inquiries, we will answer gladly by specifying the size of your company and the nature of the advertisement. The prices depend on the product relevance and can be requested by sending an e-mail to postmaster@$domainName or via the contact form.
EOT
);

SetFooter( $page, SITE_COMPANY_HTML, 'Follow us on $' );

<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: IT Hardware Support Networking.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
It's a revolution for the PC sector! We are developing the next generation of hardware support to bring the PC sector back to use!
EOT
);
$page->Save();

SetPageHeader( $page, "Comming soon...", "IT Hardware Support Networking" );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'IT Hardware Support Networking', 'Coming Soon: IT Hardware Support Networking', <<<EOT
<div class="text-center">
    <br/>
    <span class="open-sans h4">By developing solutions to connect all pc market operators, we eliminate time consuming tasks and waiting times like selecting the right hardware and accessories, over deployment, to the service search, so you can concentrate on the use of your hardware.</span>
    <hr/>
    <h3>This is the next generation! This is the comeback of the PC sector!</h3>
    Follow us and participate on something great which make IT life easier.
    <h4 class="open-sans label-horizontal">
        <small>
            We will keep you up to date on Twitter and Google+.
        </small><br/>
        Follow us: 
        <a href="https://twitter.com/Teslasoft_de"><i class="fa fa-tw"></i>@Teslasoft_de</a>
         | 
        <a title="+TeslasoftDe" rel="publisher" href="https://google.com/+TeslasoftDe"><i class="fa fa-gp"></i>+TeslasoftDe</a>
    </h4>
</div>
EOT
);

SetFooter( $page, SITE_COMPANY_HTML, 'Follow us on $' );

<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Web-Hosting.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Profitieren Sie von unserem Know-how im Bereich Web-Hosting, Kontensicherheit und gesicherter Kommunikation unter UNIX/Linux. Wir Betreiben oder richten Ihnen eine auf Ihre Bedürfnisse abgestimmte Server-Architektur ein.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Secure Web-Hosting" );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'Web-Hosting', 'Web-Hosting', <<<EOT
Der heutige Web-Hosting-Markt wird von Anbietern mit einem größtenteils sehr homogenen Angebot bestimmt. Diese Anbieter verwalten Ihre Passwörter und Ihre privaten und geschäftliche E-Mails auf ihrem Weg durch das Internet, ohne dass Sie Kenntnis davon haben wie all diese Vorgänge gesichert sind. Bis jetzt verwendet nur ein kleiner Anteil der Internet-Nutzer vorhandene, bereits seit 15 Jahren existierende, Technologien um sich vor potentiellen Sicherheitslecks zu schützen und Angriffe auf die Sicherheit abzuwehren.
<hr/>
Fünf Zeilen Code und ein ausgeklügeltes Zertifikatmanagement können schon ausreichen um einen Angriff auf ein Passwort praktisch unmöglich zu machen. Leider scheuen viele Anbieter die Verwendung von rechenintensiven Algorithmen oder Zertifikaten mit großen Schlüssellängen, weil diese Maßnahmen auf virtuellen Hosts einen hohen Kostenfaktor verursachen, da sie hohes Maß an Rechenressourcen veranschlagen die wiederum anderen Nutzern nicht zur Verfügung gestellt werden können.
<hr/>
Wir eröffnen Ihnen alle aktuell verfügbaren Technologien zur Absicherung Ihrer Infrastrukturen und geben Ihnen Empfehlungen zur Absicherung besonders kritischer Bereiche und stimmen, gemeinsam mit Ihnen, die Vor- und Nachteile bestimmter Methoden und der zu verwendenden Hardware ab.
EOT
);

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );

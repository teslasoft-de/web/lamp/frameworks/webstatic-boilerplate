<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Data Query Systems.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Sie suchen nach einem Weg, um komplexe Geschäfts- und Projektdaten einfach zu verwalten und zu analysieren? Diese Aufgaben können umständlich und zeitaufwendig sein wenn Sie unflexible Administrationswerkzeuge verwenden, die für jede Eventualität aufwendig aktualisiert und erweitert werden müssen.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Dynamic Data Management" );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'Data Query Systems', 'Datenbank-Abfragesysteme', <<<EOT
Geschäfts- und Projektdaten wachsen während des Betriebs ständig und die Struktur dieser Daten ist einer kontinuierlichen Anpassungen unterworfen, wenn Sie z. B. Ihr Angebot auf Ihre Kunden durch gewonnene Erfahrungen zuschneiden möchten oder Änderungen des heute sehr schnelllebigen Marktes Anpassungen erfordern.
<hr/>
Bei klassischen Datenbankanwendungen mit einer statischen Datenbankschicht erfordern selbst die kleinsten Änderungen der Datenbankstruktur eine zeitaufwendige Test- und Wiedereinführungsphase. Zudem können unvorhergesehene Fehler auftreten die wiederum eine sehr zeitaufwendige Fehlersuche erfordern. 
<hr/>
Wir können Ihnen aus diesem Chaos heraus helfen und bieten Ihnen eine Möglichkeit, Ihre betriebsinterne Administrationssoftware und deren Business-Objektschicht leicht erweiterbar zu entwickeln. Mit unserer innovativen O/R-Mapping-Technologie und visuellen Werkzeugen, werden Sie in die Lage versetzt, Ihre Business-Objekt-Abfragen selber zu erzeugen und zu erweitern. Die für Ihr Geschäftsmodell benötigten Daten können so vollständig dynamisch verwaltet werden!
EOT
);

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );

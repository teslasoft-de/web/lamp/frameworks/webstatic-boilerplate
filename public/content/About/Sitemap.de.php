<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 11.07.2014
 * File: Sitemap.de.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Die Webseitenstruktur von <span class="domain-name">teslasoft.de</span>.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier.", SITE_COMPANY_HTML );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-sm-12', 'Sitemap', 'Sitemap' );

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );

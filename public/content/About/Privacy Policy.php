<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 23.06.2014
 * File: Privacy Policy.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( "Privacy policy according to the German Data Protection Act." );
$page->Save();

SetPageHeader( $page, "We make IT life easier.", SITE_COMPANY_HTML );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

SetPageColumContent(
    $row,
    'col-sm-12',
    'Privacy Policy', 'Privacy Policy',
    \WebStatic\TEMPLATE_PATH . 'About/Privacy Policy/privacy-policy.phtml' );

SetFooter( $page, SITE_COMPANY_HTML, 'Follow us on $' );
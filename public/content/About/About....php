<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.06.2014
 * File: About....php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->Save();

SetAboutTitle( $page, 'About' );

$body = GetAboutBody( $page );
SetAboutContent( $body,
    'Published by ' . SITE_AUTHOR,
    'Legal Notice' );
SetAboutContent( $body );
SetAboutContent( $body,
    'This website uses a responsive design and can be viewed on any mobile device.',
    'Browser Support' );

$body->SetChild(
    'About_Frameworks', 'Frameworks', 'dt', 'Web Front-End Frameworks' );
$body->SetChild(
    'About_CMS', 'CMS', 'dt', 'Content Management System and Frameworks' );
$body->SetChild(
    'About_Footer', 'Close Button', 'button', 'Close' );
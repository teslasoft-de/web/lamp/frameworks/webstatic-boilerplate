<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 11.07.2014
 * File: Legal Notoce.de.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/


/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( "Anbieterinformationen gem. § 5 TMG" );
$page->setRobots( 'noarchive' );
$page->setGooglebot( 'noarchive' );
$page->setSlurp( 'noarchive' );
$page->setMSNBot( 'noarchive' );
$page->setTeoma( 'noarchive' );
$page->Save();

SetPageHeader( $page, "We make IT life easier.", SITE_COMPANY_HTML );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

SetPageColumContent(
    $row,
    'col-sm-12',
    'Legal Notice', 'Impressum',
    \WebStatic\TEMPLATE_PATH . 'About/Legal Notice/legal-notice.phtml' );

SetFooter( $page, SITE_COMPANY_HTML, 'Folgen Sie uns auf $' );
?>

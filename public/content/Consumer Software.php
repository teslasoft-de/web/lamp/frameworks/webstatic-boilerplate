<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Consumer Software.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Since 2004, Teslasoft is successfully developing and distributing application und utility software in the IT consumer segment. Our goal is to simplify daily tasks of our users and make their lives easier. With innovative technologies and proprietary dynamic data management solutions we are able to develop highly scalable software solutions with high performance and low maintenance costs.
EOT
);
$page->setKeywords('IT, consumer software, product development, proprietary, dynamic data management solutions, highly scalable software solutions, high performance, low maintenance costs, Windows Desktop, marketing, e-commerce, Treiber-Studio, driver update, standard devices filter, mainboard, internal peripherals, external peripherals, oem system detection, chip manufacturer drivers, OEM Driver Packages, Chipset Drivers, Driver Compilation, Operating System Switch, online hardware profile, cloud, Driver History, email alerts, profile alerts, press releases, covermount, covermount releases, covermount media, computer magazines, COMPUTER BILD, CHIP, CHIP digital, PC go!, PC Magazin, PC Welt, CD Austria, specialized trade, direct distribution, download charts, top 100 download charts, top 10 download charts, driver collection, further development, relaunch, video resume software, CV One, mobile market, IT life');
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Utility Software" );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn(
    $row, 'col-lg-12',
    'Consumer Software', 'Consumer Software' );

SetPageContent( $page, 'Consumer Software', \WebStatic\TEMPLATE_PATH . 'Consumer Software/consumer-software.phtml');

SetFooter( $page, SITE_COMPANY_HTML, 'Follow us on $' );

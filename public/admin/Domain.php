<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.01.2014
 * File: Domain.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use WebStatic\Admin\Menu;
use WebStatic\Admin\Page;
use WebStatic\Core\TemplateItem;

require_once __DIR__ . '/Domain.inc.php';

/* @var TemplateItem $menuItem */
/* @var TemplateItem $subMenu */
/* @var TemplateItem $subMenuItem */
/* @var TemplateItem $subMenuDivider */
/* @var TemplateItem $subMenuHeader */
/* @var TemplateItem $langSelect */

// Create site menu

/*----------------------------------------------------
 * Home
 *----------------------------------------------------*/
$home = new Menu( 'Home', $menuItem, 'glyphicon-home', false );
$home->Create( new Page( $homeTemplate, 'Teslasoft - We make IT life easier.' ) );

/*----------------------------------------------------
 * Consumer Software
 *----------------------------------------------------*/
$consumerSoftware = new Menu( 'Consumer Software', $menuItem, 'fa fa-cubes' );
$consumerSoftware->Create( new Page( $pageTemplate ) );

/*----------------------------------------------------
 * IT Services
 *----------------------------------------------------*/
$itServices = new Menu( 'IT Services', $subMenu, 'el el-th' );
$itServices->Create();

/*----------------------------------------------------
 * Performance Marketing
 */
$itServices->Add( 'Performance Marketing', 'fa fa-bar-chart-o', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * IT Consulting
 */
$itServices->Add( 'IT-Consulting', 'fa fa-users', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * Web Hosting
 */
$itServices->Add( 'Web-Hosting', 'fa fa-cogs', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * Data Query Systems
 */
$itServices->Add( 'Data Query Systems', 'fa fa-database', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * IT Hardware Support Networking
 */
$itServices->Add( 'IT Hardware Support Networking', 'el el-network', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * Contact
 *----------------------------------------------------*/
$contact = new Menu( 'Contact', $menuItem, 'glyphicon-envelope' );
$contact->Create( new Page( $contactTemplate, 'Contact Us - ' . SITE_NAME, false ) );

/*----------------------------------------------------
 * Info
 *----------------------------------------------------*/
$info = new Menu( 'About', $subMenu, 'glyphicon-info-sign', null );
$info->Create();

/*----------------------------------------------------
 * Legal Notice
 */
$info->Add( 'Legal Notice', 'fa fa-gavel', $subMenuItem, $pageTemplate, null, false );

/*----------------------------------------------------
 * Privacy Policy
 */
$info->Add( 'Privacy Policy', 'glyphicon-eye-open', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * Help
 */
$info->AddMenu( new Menu( 'Help_Divider', $subMenuDivider, null, null ) );
$info->AddMenu( new Menu( 'Help', $subMenuHeader, 'fa fa-life-ring fa-fw', null ) );

/*----------------------------------------------------
 * Sitemap
 */
$info->Add( 'Sitemap', 'fa fa-sitemap', $subMenuItem, $pageTemplate );

/*----------------------------------------------------
 * Browser Support
 */
$browserSupport = new Menu( 'Browser Support', $subMenuItem, 'fa fa-photo', false );
$browserSupport->QueryPath = '#browser-support';
$info->AddMenu( $browserSupport, $wbsTemplate );

/*----------------------------------------------------
 * About
 */
$info->AddMenu( new Menu( 'About_Divider', $subMenuDivider, null, null ) );

$about = new Menu( 'About...', $subMenuItem, 'fa fa-info', false );
$about->QueryPath = '#about';
$info->AddMenu( $about, $aboutTemplate, 'Über ' . SITE_NAME );

/*----------------------------------------------------
 * Language Select
 *----------------------------------------------------*/
$languageSelect = new Menu( 'Language Select', $langSelect, null, null );
$languageSelect->Create();
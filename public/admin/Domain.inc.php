<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.01.2014
 * File: Domain.inc.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

require_once __DIR__ .  '/../WebStatic/Admin/Include.inc.php';
use AppStatic\Configuration\System;
use AppStatic\Web\DomainName;
use WebStatic\Core\Template;

Template::$TemplateMode = true;
// Create template items
require_once WebStatic\TEMPLATE_PATH . 'include.inc.php';
require_once WebStatic\TEMPLATE_PATH . 'site.php';
require_once WebStatic\TEMPLATE_PATH . 'Home/home.php';
require_once WebStatic\TEMPLATE_PATH . 'page.php';
require_once WebStatic\TEMPLATE_PATH . 'Contact/contact.php';
require_once WebStatic\TEMPLATE_PATH . 'About/about.php';
require_once WebStatic\TEMPLATE_PATH . 'About/Browser Support/browser-support.php';

// Create the domain
$domainName = System::IsLocal() ? DomainName::getCurrent() : new DomainName( SITE_DOMAIN );
global $domain;
$domain = CreateDomain( $domainName );

// Create site for the domain
global $site;
$site = $domain->SetSite( $domainName, $siteTemplate, 'Teslasoft Company', DOCUMENT_ROOT, SITE_LANGUAGE );

$menuItem       = $siteTemplate->MenuItem;
$subMenu        = $siteTemplate->SubMenu;
$subMenuItem    = $siteTemplate->SubMenuItem;
$subMenuDivider = $siteTemplate->SubMenuItem_Divider;
$subMenuHeader  = $siteTemplate->SubMenuItem_Header;
$langSelect     = $siteTemplate->LanguageSelect;
<?php
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 08.04.2017
 * File: Language.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

namespace Common;

use WebStatic\Common\PropertyDictionary;

class Language extends \WebStatic\Common\Language
{
    /**
     * @var PropertyDictionary
     */
    public static $Content;

    /**
     * Language constructor.
     */
    public function __construct() {
        parent::__construct();

        $this->Load('Content');
    }
}

Language::Register();
